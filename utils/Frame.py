#!/usr/bin/python 
# -*- coding: utf-8 -*-

from __future__ import division 
import sys 
import inspect 
import getopt 
import operator 

"""
 Créé le mercredi 25 février 2015.

 représente une frame et ses rôles

"""

class Role(object):
    
    def __init__(self, role, lemma, PoS):
        self.role = role
        self.lemma = lemma
        self.PoS = PoS


class RoleWithPath(Role):
    def __init__(self,role,lemma,PoS,path,position=None):
        Role.__init__(self,role,lemma,PoS)
        self.path=path
        self.position=position

class Frame(object):
    """ définit une occurrence de cadre, avec sa syntaxe propre """
    
    def __init__(self,frame,trigger,PoS):
        self.name=frame
        self.trigger=trigger
        self.PoS=PoS
        self.roles=[]
 
    def addRole(self,role,lemma,PoS):
        self.roles.append(Role(role,lemma,PoS))

    def addRoles(self,roleList):
        self.roles.extend(roleList)
    
    def __str__(self):
        return '{} {} {}'.format(self.trigger, self.PoS, len(self.roles))    
 
class FrameWithPathes(Frame):
    def __init__(self,frame,trigger,PoS):
        Frame.__init__(self,frame,trigger,PoS)

    def addRole(self,role,lemma,PoS,path,position=None):
        self.roles.append(RoleWithPath(role,lemma,PoS,path,position))

if __name__ == "__main__":
    #if debug: print inspect.stack()[0][3]
    print >>sys.stderr, sys.argv[0], "ne peut être utilisé en standalone"
    
