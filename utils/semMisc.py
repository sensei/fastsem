#!/usr/bin/python 
# -*- coding: utf-8 -*-

import sys 
import inspect 
import gzip
"""
 Créé le lundi 21 septembre 2015.

 diverses fonctions pour le traitement de la sémantique

"""

def coarsePoSEn(pos):

   if pos in ("``",",",":",".","''","(",")","$","#"):
      return 'P'

   if pos in ("JJ","JJR","JJS"):
      return 'JJ'

   if pos in ("VB","VBD","VBG","VBN","VBP","VBZ"):
      return "V"

   if pos in ("NN","NNP","NNPS","NNS"):
      return "NN"

   if pos in ("PRP","PRP$"):
      return "PRP"

   if pos in ("RB","RBR","RBS"):
      return "RB"

   return pos

def coarsePoSFr(pos):
   
   if pos in ("ADJ","ADJWH"):
      return "ADJ"

   if pos in ("ADV","ADVWH"):
      return "ADV"

   if pos in ("CC","CS"):
      return "C"

   if pos in ("CLO","CLR","CLS"):
      return "CL"

   if pos in ("DET","DETWH"):
      return "D"

   if pos in ("NC","NPP"):
      return "N"

   if pos in ("PRO","PROREL","PROWH"):
      return "PRO"

   if pos in ("V","VIMP","VINF","VPP","VPR","VS"):
      return "V"
      
   return pos

def coarsePoS(pos,lang):
   if lang=="en":
      return coarsePoSEn(pos)
   elif lang=="fr":
      return coarsePoSFr(pos)

def read_LU(file_lu,withPos=True):
   dic_lu={}
   with open(file_lu,'r') as stream:
      for line in stream.readlines():
         l=line.replace('\n','').split("\t")
         # if withPos and  l[0].split(".")[1] not in ("a","n","v"):              #ici on ne teste que 3 POS
         #     continue
         dic_lu[l[0]]=list(set(l[1:]))
   return dic_lu 



def lexicalUnitizationEN(lemme,pos):
   if pos in ('JJ','JJR','JJS','LS'):
      lemme=lemme+".a"
   elif pos in ('NN','NNP','NNPS','NNS'):
      lemme=lemme+".n"
   elif pos in ('VB','VBD','VBG','VBN','VBP','VBZ','MD'):
      lemme=lemme+".v"
   elif pos in ('RB','RBR','RBS','WRB'):
      lemme=lemme+".adv"
   elif lemme in ("much","some","a few","little"):
      lemme=lemme+".art"
   elif lemme in ("so","since","for","because","while","but") :
      lemme=lemme+".c"
   elif lemme in ("yoohoo","hey","sh") :
      lemme=lemme+".intj"
   elif lemme in ("450","forty","six","fifty","four","fifteen","twenty","hundred","seventy","6500","ten","1200","eight","ninety","1000","seventy-four","five hundred","seven","two hundred","sixty","five","twenty-five","twenty-one","two","fifty-two","one","twelve","400","million","three","zero","billion","thirty"):
      lemme=lemme+".num"
   elif lemme in ("while","although"):
      lemme=lemme+".scon"
   elif pos in ('IN','TO') :
      lemme=lemme+".prep"
   else:
        lemme=lemme

   return lemme

def lexicalUnitization(lemme,pos,lang="en"):
   if lang=="en":
      return lexicalUnitizationEN(lemme,pos)

   return lemme



def openInStream(file_in,stdin=sys.stdin):
    if file_in==stdin:
        return sys.stdin
    if file_in.split('.')[-1]=='gz':
        return gzip.open(file_in,'rb')
    return open(file_in,'r') 

def openOutStream(file_out,stdout=sys.stdout,append=False):
    if file_out==stdout:
        return sys.stdout
    if file_out.split('.')[-1]=='gz':
        return gzip.open(file_out,'ab' if append else 'wb')
    return open(file_out,'a' if append else 'w')

class FileManager(object):
    def __init__(self,filename,std='sys.stdin',append=False,mode='r'):
        self.filename=filename
        self.std=std
        self.append=append
        self.mode=mode

    def __enter__(self):
        if 'r' in self.mode:
            self.stream=openInStream(self.filename,stdin=self.std)
        elif 'w' in self.mode:
            self.stream=openOutStream(self.filename,stdout=self.std,append=self.append)
        else:
            self.stream=open(self.filename,self.mode)
        return self.stream

    def __exit__(self, type, value, traceback):
        if self.filename!=self.std:
            self.stream.close()

def mylogging(func):
    """
    Un décorateur qui log l'activité d'un script.
    (Ok, en vrai ça fait un print, mais ça pourrait logger !)
    """
    def wrapper(*args, **kwargs):
        print >>sys.stderr, "on entre dans ", func.__name__
        res = func(*args, **kwargs)
        print >>sys.stderr, "on sort de ", func.__name__
        return res
    return wrapper


def fancyWaitingPrint(total,current,size=20,symbol='='):
    n=int((current/(total+0.0))*size)
    barre='='*n+' '*(size-n)
    print >>sys.stderr, '\r', '['+"".join(barre)+']', '\tfichier', int(current), '/', total,
    sys.stderr.flush()
    
