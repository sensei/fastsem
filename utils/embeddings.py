#!/usr/bin/python 
# -*- coding: utf-8 -*-

import sys 
import inspect 
import getopt 
from math import ceil
"""
 Créé le mardi  7 avril 2015.

 classes et fonctions utiles pour jouer avec des embeddings

"""
    
def ReadEmbeddingsFromStream(stream=sys.stdin,frame_list=[],role_list=[],word_list=[],nbClasses=None):
    def indexOf(value):
        try:
            return role_list.index(value)
        except ValueError:
            return None

    subclass=lambda x: ceil(x*nbClasses) if nbClasses else x

    precLineisEmpty=True
    embeddingsScoreByFrameRoleWordTuple={}
    
    for line in stream:
        l=line.strip().split()
        if len(l)==0:
            precLineisEmpty=True
            continue
        if precLineisEmpty:
            precLineisEmpty=False
            try:
                frame=frame_list.index(l[0])
            except ValueError:
                frame=None
            roles=map(indexOf,l[1:]) 
        else:
            if frame==None:
                continue
            try:
                word=word_list.index(l[0])
            except ValueError:
                word_list.append(l[0])
                word=len(word_list)
            scores=map(float,l[1:])
            for i in xrange(len(roles)):
                if not roles[i]:
                    continue
                embeddingsScoreByFrameRoleWordTuple[(frame,roles[i],word)]=subclass(scores[i])

    return embeddingsScoreByFrameRoleWordTuple


if __name__ == "__main__":
    print >>sys.stderr , "ne s'utilise pas en standalone"
