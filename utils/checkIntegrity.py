#!/usr/bin/python 
# -*- coding: utf-8 -*-

from __future__ import division 
import sys 
import inspect 
import getopt 
import operator 

"""
 Créé le mardi 17 février 2015.

 vérifie l'intégrité de ce qui a été généré par le stanford parser.

"""
#ROOT="root"
#LABELDEP=7
#HEAD=6
ROOT="ROOT"
LABELDEP=5
HEAD=4
mysplit=lambda x: map(lambda y : y.strip(),x.split("\t"))

def HasRoot(sent):
    for k in sent:
        if sent[k][LABELDEP]==ROOT:
            return True
    return False

def CountRoots(sent):
    nb=0
    for k in sent:
        if sent[k][LABELDEP]==ROOT:
            nb+=1
    return nb

def HasCycle(sent):
    for k in sent:
        i=0
        head=k
        while(i<len(sent) and head!="0"):
            head=sent[head][HEAD]
            i+=1

        if head!='0':
            # print k, head
            return True

    return False

def printSentence(sent):
    for i in xrange(1,len(sent)+1):
        print "\t".join(sent[str(i)])
    print

def main():
    noRoot=0
    nbMultiRoot=0
    cycle=0
    lenSentWithCycle=0.0
    lenSentWithoutRoot=0.0
    multiRoot=0.0
    noRootAndCycle=0
    nbSent=0
    conllSentence={}
    for l in sys.stdin:
        l=mysplit(l)
        if len(l)>1:
            conllSentence[l[0]]=l
        else:
            nbSent+=1
            flag=False
            if (not HasRoot(conllSentence)):
                noRoot+=1
                flag=True
                lenSentWithoutRoot+=1
                # printSentence(conllSentence);
                # exit()
            else: 
                nbRoot=CountRoots(conllSentence);
                if nbRoot>1: 
                    nbMultiRoot+=1
                    multiRoot+=nbRoot
            if (HasCycle(conllSentence)):
                cycle+=1
                lenSentWithCycle+=len(conllSentence)
                if flag:
                    noRootAndCycle+=1
                # printSentence(conllSentence)
            conllSentence={}

       
    if noRoot or cycle or nbMultiRoot:
        print noRoot, lenSentWithoutRoot/noRoot, cycle, lenSentWithCycle/cycle, noRootAndCycle, nbMultiRoot, multiRoot/nbMultiRoot, nbSent


if __name__ == "__main__":
    #if debug: print inspect.stack()[0][3]
    sys.exit(main())
