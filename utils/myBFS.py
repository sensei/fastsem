#!/usr/bin/python 
# -*- coding: utf-8 -*-

import sys 
import inspect 
import begin 
from Queue import Queue
import Chemins
"""
 Créé le jeudi 10 décembre 2015.

 implémentation ad-hoc de l'algorithme de Breadth First Search, pour trouver tous les plus courts chemins entre un point et tous les autres dans un graphe acyclique

"""
def hasUnprotectedSlash(path):
    if not "/" in path:
        return False
    path=path[::-1]
    rb=0
    for c in path:
        if c==')':
            rb+=1
        if c=='(':
            rb-=1
        if rb>1:
            return False
        if c=='/':
            return True
    
def removeExtraBraces(path):
    # if path=='()':
    #     return path
    bracesToRemove=0
    lb=0
    expr=[False]
    for c in path:
        if c=='(':
            lb+=1
            if len(expr) <= lb:
                expr.append(False)
        elif c not in '()':
            expr[lb]=True
        elif c==")":
            if expr[lb]:
                expr[lb]=False
                lb-=1
            else:
                bracesToRemove+=1

    if bracesToRemove:
        return path[bracesToRemove:-bracesToRemove]
    return path


class Vertex:
    def __init__(self, node):
        self.node=node
        self.distance=None
        self.parents=[]
        self.pathes=[]
        self.adjacent={}

    def initAgain(self):
        self.distance=None
        self.parents=[]
        self.pathes=[]

    def add_neighbor(self, neighbor, label):
        try:
            self.adjacent[neighbor] += "/"+label
            self.adjacent[neighbor] = '/'.join(sorted(self.adjacent[neighbor].split('/'),key=lambda x: x.replace("-",'')))
        except KeyError:
            self.adjacent[neighbor] = label

    def get_connections(self):
        return self.adjacent.keys()  

    def add_parent(self,parent):
        self.parents.append(parent)
        parentPath=parent.pathToString()
        if parentPath=='()':
            self.pathes.append("("+self.adjacent[parent]+")")
            return

        self.pathes.append(parentPath[:-1]+","+self.adjacent[parent]+")")

    def pathToString(self):
        if self.distance==None:
            return '(__no_path__)'
        if self.distance==0:
            return '()'
        return removeExtraBraces("("+"/".join(sorted(self.pathes,key=lambda x: x.replace("-",'')))+")")

    def __str__(self):
        return 'vertex: {} parents: {} distance={} adjacent:{} path={} '.format(self.node,map(str,(x.node for x in self.parents)),self.distance,map(str,(x.node for x in self.adjacent.keys())),self.pathToString())

def oppositePolarity(string):
    splitted=string.split('/')
    toReturn=''
    for elt in splitted:
        if elt.startswith('-'):
            toReturn+=elt[1:]
        else:
            toReturn+="-"+elt
        toReturn+="/"
    return toReturn[0:-1]

class Graph:
    def __init__(self):
        self.vert_dict = {}
        self.num_vertices = 0
        self.root=None

    def __iter__(self):
        return iter(self.vert_dict.values())

    def __str__(self):
        return "\n".join(map(str,self.vert_dict.values()))

    def add_vertex(self, node):
        self.num_vertices = self.num_vertices + 1
        new_vertex = Vertex(node)
        self.vert_dict[node] = new_vertex
        return new_vertex

    def get_vertex(self, n):
        if n in self.vert_dict:
            return self.vert_dict[n]
        else:
            return Vertex(n)

    def add_edge(self, frm, to, label):
        if frm not in self.vert_dict:
            self.add_vertex(frm)
        if to not in self.vert_dict:
            self.add_vertex(to)

        self.vert_dict[frm].add_neighbor(self.vert_dict[to], oppositePolarity(label))
        self.vert_dict[to].add_neighbor(self.vert_dict[frm], label)

    def get_vertices(self):
        return self.vert_dict.keys()

    def initAgain(self,root):
        for v in self.vert_dict:
            self.vert_dict[v].initAgain()
        self.root=root

def makeGraph(sentence,splitted=False,govCol=4,labelCol=5,deep=True):
    mysplit=lambda x: map(lambda y : y.strip(),x.split("\t"))
    splitLine=(lambda x: x) if splitted else (lambda x: mysplit(x))
    canonicalLabel=lambda x: x.split(":")[-1]

    g=Graph()

    for i in sentence:
        line=splitLine(sentence[i])
        if deep:
            dd=Chemins.deepDependencies(line[govCol],line[labelCol])
            for gov,label in zip(dd[0],dd[1]):
                if gov=='0':
                    continue
                g.add_edge(gov,i,label)
        else:
            gov,label=Chemins.surfaceDependency(line[govCol],line[labelCol])
            if gov=='0':
                continue
            g.add_edge(gov,i,label)
        
    
    return g

def BFS(graph,root):
    
    graph.initAgain(root)
    q=Queue()
    root=graph.get_vertex(root)
    root.distance=0
    q.put(root)

    while not q.empty():
        current=q.get()
        for n in current.get_connections():
            if n.distance==None:
                n.distance=current.distance+1
                n.add_parent(current)
                q.put(n)
            elif n.distance == current.distance+1:
                n.add_parent(current)
        

@begin.start
def main():
    pass
    # sentence={'1':["1","0","root"],'2':['2','1|4','a|f'],'3':['3','1','b'],'4':['4','3|2','c|d'],'5':['5','4','e'],'6':['6','7','g']}
    # g=makeGraph(sentence,splitted=True,govCol=1,labelCol=2)
    # BFS(g,"1")
    # for i in sentence:
    #     print g.get_vertex(i)
    # BFS(g,'4')
    # for i in sentence:
    #     print g.get_vertex(i)
    # BFS(g,'6')
    # for i in sentence:
    #     print g.get_vertex(i)
