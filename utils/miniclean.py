#!/usr/bin/python 
# -*- coding: utf-8 -*-

import sys 
import inspect 
import getopt 
import checkIntegrity as ci
import conllIO as lc
"""
 Créé le jeudi 26 février 2015.

 

"""
# pour enlever les lignes blanches mutliples
def removeEmptyLines(stream):
    precIsEmpty=False
    for l in stream:
        l2=l.split()
        if len(l2):
            precIsEmpty=False
            print l, 
            continue
        if not precIsEmpty:
            print 
            precIsEmpty=True
        

# pour enlever les phrases avec boucles
def removeSentencesWithLoops():
    it=lc.SentenceIteratorFromConll(sys.stdin)
    for sent in it:
        if not ci.HasCycle(sent):
            lc.printSentence(sent,sys.stdout)

def main():
    # pour enlever les lignes blanches mutliples
    # removeEmptyLines()

    # pour enlever les phrases avec boucles
    removeSentencesWithLoops()
    

            
if __name__ == "__main__":
    #if debug: print inspect.stack()[0][3]
    sys.exit(main())
