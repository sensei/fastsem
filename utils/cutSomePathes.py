#!/usr/bin/python 
# -*- coding: utf-8 -*-

from __future__ import division 
import sys 
import inspect 
import getopt 
import operator 
try:
   import cPickle as pickle
except:
   import pickle


"""
 Créé le lundi 11 août 2014.

 permet de diminuer la sortie de preperceptron.py pour accélérer le perceptron
on en profite aussi pour ne sélectionner que les parties de discours qui nous intéressent

"""

class Role(object):
    """
    """
    
    def __init__(self, role, lemme, PoS, chemin):
        self.role = role
        self.lemme = lemme
        self.PoS = PoS
        self.chemin = chemin


class Frame(object):
    """ définit une occurrence de cadre, avec sa syntaxe propre """
     
    def __init__(self,frame,trigger,PoS):
        self.f=frame
        self.t=trigger
        self.p=PoS
        self.r=[]

    def addRole(self,role,lemme,PoS,chemin):
        self.r.append(Role(role,lemme,PoS,chemin))
    

def FrequentPathesInFic(fic,pathFrequency,pathFrequencyByPos,rnul="__rnul__",fnul='__fnul__'):
    f=open(fic,"r")
    
    allPos=set()
    
    for l in f:
        l=l.strip().split('\t')
        if l[0]==fnul:
           continue
        framePos=l[2]
        allPos.add(framePos)
        pathFrequencyByPos.setdefault(framePos,{})
        for i in range(3,len(l)):
            if (i-3)%4==0:
                role=l[i]
            elif (i-3)%4==2:
                PoS=l[i]
                allPos.add(PoS)
            elif (i-3)%4==3:
                path=l[i]
                if role!="__rnul__":
                    pathFrequency.setdefault(path,0)
                    pathFrequency[path]+=1
                    pathFrequencyByPos[framePos].setdefault(path,0)
                    pathFrequencyByPos[framePos][path]+=1

    f.close()
    return allPos

def InteractiveChoice(allPos):
    r=raw_input("Effectuer les traitements en fonction de la partie de discours? O/N ")
    while( r not in ("O","N","o","n")):
        r=raw_input("Effectuer les traitements en fonction de la partie de discours? O/N ")
    isByPoS=r.upper()=="O"
    
    r=raw_input("Effectuer les traitements en par nombre d'occurrence minimal ou prendre les n plus fréquents? 1/2 ")
    while(r not in ("1","2")):
        r=raw_input("Effectuer les traitements en par nombre d'occurrence minimal ou prendre les n plus fréquents? 1/2 ")
    isByFreq=r=="2"

    r=raw_input("Quelle valeur pour ce nombre? ") 
    keyVal=int(r)

    r=raw_input("Choisir quelles parties de discours traiter pour les triggers? O/N ") 
    while(r not in ("O","N","o","n")):
        r=raw_input("Choisir quelles parties de discours traiter pour les triggers? O/N ") 
    if r.upper()=="N":
        return isByPoS,isByFreq,keyVal,allPos
    
    selectedPos=set()
    for p in allPos:
        r=raw_input("Considérer "+p+" ? O/N ")
        while(r not in ("O","N","o","n")):
            r=raw_input("Considérer "+p+" ? O/N ")
        if r.upper()=="O":
            selectedPos.add(p)
    
    return isByPoS,isByFreq,keyVal,selectedPos

def sortDicDic(dicdic):
    dicSortedLists={}
    for k in dicdic:
        dicSortedLists[k]=sorted(dicdic[k].items(),key=lambda x: x[1],reverse=True)
    return dicSortedLists        

def CutSomePathes(fic_in,fic_out,isByPoS,isByFreq,keyVal,posToConsider,sortedList,sortedListByPos,useNum=False,rnul="__rnul__",fnul='__fnul__',prune=False):

    stream_in=open(fic_in,"r")
    stream_out=open(fic_out,"w")

    refArray=[]
    totFrameForPOS=0
    totrolesnnuls=0
    totroles=0
    totrolesForPOS=0
    totrolesnnulsForPOS=0
    for line in stream_in:

        l=line.strip().split('\t')
        
        ref=Frame(l[0],l[1],l[2])
        totFrameForPOS+=1 if ref.p in posToConsider else 0

        for i in range(3,len(l)):
            if (i-3)%4==0:
                role=l[i]
                totroles+=1
                totrolesForPOS+=1 if ref.p  in posToConsider else 0
                if role!=rnul:
                    totrolesnnuls+=1
                    totrolesnnulsForPOS+=1 if ref.p  in posToConsider else 0
            elif (i-3)%4==1:
                acteur=l[i]
            elif (i-3)%4==2:
                PoS=l[i]
            else:
                chemin=l[i]
                ref.addRole(role,acteur,PoS,chemin)

        refArray.append(ref)

    print >>sys.stderr, "\nPour le fichier entier, on a ",len(refArray), "phrases lues, et ", totroles, " roles, dont", totrolesnnuls, "rôles non nuls"
    print >>sys.stderr, "Pour les parties de discours demandées, on a",totFrameForPOS, "phrases lues, et ", totrolesForPOS, " roles, dont", totrolesnnulsForPOS, "rôles non nuls"


    nbDeRolesParPos={}
    nbDappdePos={}

    if isByPoS:
        topList={}
        if isByFreq:
            for k in sortedListByPos:
                topList[k]=[path for path,num in sortedListByPos[k][:keyVal]]
        else:
            for k in sortedListByPos:
                topList[k]=[path for path,num in sortedListByPos[k] if num>keyVal]
    else:
        if isByFreq:
            topList=[path for path,num in sortedList[:keyVal]]
        else:
            topList=[path for path,num in sortedList if num>keyVal]



    
    with open("pathesToconsider.olv","w") as f:
        pickle.dump(isByPoS,f)
        pickle.dump(topList,f)


    # restitution des données en épurant
    used=[]
    totroles=0
    totrolesnnuls=0
    for ref in refArray:
        if ref.p not in posToConsider:
            continue
        if useNum:
            used.append(refArray.index(ref))
        toPrint=ref.f
        toPrint+="\t"
        toPrint+=ref.t
        toPrint+="\t"
        toPrint+=ref.p
        x=0
        for r in ref.r:
            #print topList.keys()
            try:
                if (isByPoS and r.chemin in topList[ref.p]) or (not isByPoS and r.chemin in topList) or not prune:
                    x+=1
                    totroles+=1
                    if r.role!="__rnul__":
                        totrolesnnuls+=1
                    toPrint+="\t"
                    toPrint+=r.role
                    toPrint+="\t"
                    toPrint+=r.lemme
                    toPrint+="\t"
                    toPrint+=r.PoS
                    toPrint+="\t"
                    toPrint+=r.chemin
            except KeyError:
                continue
        # if x==0:
        #     continue
        toPrint+="\n"
        nbDeRolesParPos.setdefault(ref.p,0)
        nbDeRolesParPos[ref.p]+=x
        nbDappdePos.setdefault(ref.p,0)
        nbDappdePos[ref.p]+=1
        stream_out.write(toPrint)

    print >>sys.stderr,  totroles ,"roles après épuration, dont ", totrolesnnuls, "rôles non nuls"
    
    for p in nbDappdePos.keys():
        print >>sys.stderr, p, " : moyenne des chemins par occurrence: ",nbDeRolesParPos[p]/nbDappdePos[p], "occ :", nbDappdePos[p]

    if useNum:
        with open(useNum,"a") as f:
            pickle.dump(used,f)

    stream_in.close()
    stream_out.close()

def afficheAide():
    print >>sys.stderr, "usage: ",sys.argv[0], "--train ficTrain [--test ficTest] [-I] [--num]"
    print >>sys.stderr, "\t-I : Mode interactif pour le choix des chemins fréquents"
    print >>sys.stderr, "\t--num file : Permet d'afficher la liste des indices des phrases sélectionnées dans un fichier"
    print >>sys.stderr, "\t--isByPoS : Compare les chemins en fonction de la partie de discours du trigger"
    print >>sys.stderr, "\t--isByFreq : Effectue le classement par fréquence des chemins ou par nombre d'occurrence si non spécifié"
    print >>sys.stderr, "\t--keyVal val : valeur de référence pour la fréquence ou le nombre d'occurrence (1 par défaut)"
    print >>sys.stderr, "\t--posToConsider [V|N|A|all] : ensemble des parties de discours à spécifier (all par défaut)"
    print >>sys.stderr, "\t--pruneTrain : élague le train si spécifié"
    print >>sys.stderr, "\t--pruneTest : élague le test si spécifié"
    print >>sys.stderr, "\t--lang en|fr : langue (en par défaut)"

    exit()

def gestionArguments():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hI", ["help","train=","test=","num=","isByPoS","isByFreq","keyVal=","posToConsider=","pruneTrain","pruneTest","lang="])
    except getopt.GetoptError:
        # print help information and exit:
        afficheAide()

    fic_in=fic_out=fic_in2=fic_out2=None
    interactive=False
    num=None

    isByPoS=False
    isByFreq=False
    keyVal=1
    posToConsider='all'
    pruneTrain=False
    pruneTest=False
    lang="en"



    for o,a in opts:
        if o in ("-h", "--help"):
            afficheAide()
        if o == "--train":
            fic_in=a
        if o == "--test":
            fic_in2=a
        if o == "-I":
            interactive=True
        if o == "--num":
            num=a
        if o == "--isByPoS":
            isByPoS=True
        if o == "--isByFreq":
            isByFreq=True
        if o == "--keyVal":
            keyVal=int(a)
        if o == "--posToConsider":
            if 'all' not in a and 'V' not in a and "N" not in a and "A" not in a:
                afficheAide()
            posToConsider=a
        if o == "--pruneTrain":
            pruneTrain=True
        if o == "--pruneTest":
            pruneTest=True
        if o == "--lang":
            if a not in ("fr","en"):
                print >>sys.stderr, "langue inconnue"
                afficheAide()
            lang=a

    fic_out=fic_in+"Cut"
    if fic_in2:
        fic_out2=fic_in2+'Cut'

    return fic_in,fic_out,fic_in2,fic_out2,interactive,num,isByPoS,isByFreq,keyVal,posToConsider,pruneTrain,pruneTest,lang

def main():

    fic_in,fic_out,fic_in2,fic_out2,isInteractive,useNum,isByPoS,isByFreq,keyVal,posSet,pruneTrain,pruneTest,lang=gestionArguments()


    pathFrequencyByPos={}
    pathFrequency={}

    allPos = FrequentPathesInFic(fic_in,pathFrequency,pathFrequencyByPos)
    # if fic_in2:
    #     allPos|= FrequentPathesInFic(fic_in2 ,pathFrequency,pathFrequencyByPos)

    # # choix:
    # ## Appliquer les choix suivants par PoS:
    # isByPoS=True
    # ## Fonctionner par nombre d'occurrence minimal ou prendre les plus fréquents:
    # isByFreq=False
    # ## Valeur pour le nombre minimal ou les n premiers:
    # keyVal=1
    ## Ne considérer que les triggers étant des PoS suivantes:

   
    if posSet=="all":
        posToConsider= allPos      #allPos        # set()
    else :
        posToConsider= set()
        if lang=="en":
            if "A" in posSet:
                posToConsider.add('JJ')
                posToConsider.add('JJS')
                posToConsider.add('JJR')
            if "N" in posSet:
                posToConsider.add('NN')
                posToConsider.add('NNS')
                posToConsider.add('NNP')
                posToConsider.add('NNPS')
            if "V" in posSet:
                posToConsider.add('VBG')
                posToConsider.add('VBD')
                posToConsider.add('VBN')
                posToConsider.add('VBP')
                posToConsider.add('VBZ')
                posToConsider.add('VB')
        elif lang=="fr":
            if "V" in posSet:
                posToConsider.add('V')
                posToConsider.add('VIMP')
                posToConsider.add('VPP')
                posToConsider.add('VPR')
                posToConsider.add('VINF')
                posToConsider.add('VS')
            if "N" in posSet:    
                posToConsider.add('NC')
                posToConsider.add('NPP')
            if "A" in posSet:
                posToConsider.add('ADJ')
                posToConsider.add('ADJWH')
    



    if isInteractive:
        isByPoS,isByFreq,keyVal,posToConsider=InteractiveChoice(allPos)
    

    print >>sys.stderr, "Configuration actuelle:"
    if isByPoS:
        print >>sys.stderr, "\ten fonction des PoS: (",",".join(posToConsider),")"
    if isByFreq:
        print >>sys.stderr, "\ten fonction de la fréquence: les ",keyVal, " plus fréquents"
    else: 
        print >>sys.stderr, "\tavec au moins ", keyVal, " apparitions"
   


    sortedListByPos=sortDicDic(pathFrequencyByPos)
    sortedList=sorted(pathFrequency.items(),key=lambda x: x[1],reverse=True)
    
    if useNum:
        with open(useNum,"w") as f:
            pass

    CutSomePathes(fic_in,fic_out,isByPoS,isByFreq,keyVal,posToConsider,sortedList,sortedListByPos,useNum,prune=pruneTrain)
    if fic_in2:
        CutSomePathes(fic_in2,fic_out2,isByPoS,isByFreq,keyVal,posToConsider,sortedList,sortedListByPos,useNum,prune=pruneTest)
    


    # # initialisation des dicos et listes:
    # refArray=[]
    # chemins={}
    # cheminsParPos={}
    # partsOfSpeech={}
    # totroles=0
    # totrolesnnuls=0
    # cheminsValides=set()
    # cheminsValidesByPos={}
    # cheminsValidesByPos["adj"]=("(-NMOD)","()","(AMOD)","(-NMOD,NMOD)","(-PRD,SBJ)")
    # cheminsValidesByPos["ver"]=("(OBJ)","(SBJ)","(-VC,SBJ)","(ADV)","(OPRD)")
    # cheminsValidesByPos["nom"]=("(NMOD)","()","(LOC)","(-NMOD,NMOD)","(-OBJ,SBJ)")
    
    # posToCle={}
    # posToCle['JJ']='adj'
    # # posToCle['JJR']='adj'
    # # posToCle['JJS']='adj'
    # # posToCle['LS']='adj'
    # posToCle['NN']='nom'
    # posToCle['NNS']='nom'
    # posToCle['VB']='ver'
    # # posToCle['VBD']='ver'
    # # posToCle['VBG']='ver'
    # # posToCle['VBN']='ver'
    # # posToCle['VBP']='ver'
    # # posToCle['VBZ']='ver'
    # # posToCle['MD']='ver'

    # # lecture des données
    # for line in sys.stdin.readlines():
    #     l=line.strip().split('\t')
    #     ref=Frame(l[0],l[1],l[2])

    #     for i in range(3,len(l)):
    #         if (i-3)%4==0:
    #             role=l[i]
    #             totroles+=1
    #             if role!="__rnul__":
    #                 totrolesnnuls+=1
    #         elif (i-3)%4==1:
    #             acteur=l[i]
    #         elif (i-3)%4==2:
    #             PoS=l[i]
    #             if role!="__rnul__":
    #                 partsOfSpeech.setdefault(PoS,0)
    #                 partsOfSpeech[PoS]+=1
    #         else:
    #             chemin=l[i]
    #             chemins.setdefault(chemin,0)
    #             chemins[chemin]+=1
    #             cheminsParPos.setdefault(PoS,{})
    #             cheminsParPos[PoS].setdefault(chemin,0)
    #             cheminsParPos[PoS][chemin]+=1
    #             ref.addRole(role,acteur,PoS,chemin)
    #             if role!="__rnul__":
    #                 cheminsValides.add(chemin)

    #     if ref.p not in posToCle.keys():
    #         continue   
    #     refArray.append(ref)

    # print >>sys.stderr, "\t",len(refArray), "phrases lues, et ", totroles, " roles, dont", totrolesnnuls, "rôles non nuls"

    # nbDeRolesParPos={}
    # nbDappdePos={}

    # # restitution des données en épurant
    # totroles=0
    # totrolesnnuls=0
    # for ref in refArray:
    #     toPrint=ref.f
    #     toPrint+="\t"
    #     toPrint+=ref.t
    #     toPrint+="\t"
    #     toPrint+=ref.p
    #     x=0
    #     for r in ref.r:
    #         if r.chemin in cheminsValidesByPos[posToCle[ref.p]]: #cheminsParPos[r.PoS][r.chemin]>1 and r.chemin in cheminsValides: #if r.chemin in cheminsValidesByPos[posToCle[ref.p]]:
    #             x+=1
    #             totroles+=1
    #             if r.role!="__rnul__":
    #                 totrolesnnuls+=1
    #             toPrint+="\t"
    #             toPrint+=r.role
    #             toPrint+="\t"
    #             toPrint+=r.lemme
    #             toPrint+="\t"
    #             toPrint+=r.PoS
    #             toPrint+="\t"
    #             toPrint+=r.chemin
    #     # if x==0:
    #     #     continue
    #     toPrint+="\n"
    #     nbDeRolesParPos.setdefault(ref.p,0)
    #     nbDeRolesParPos[ref.p]+=x
    #     nbDappdePos.setdefault(ref.p,0)
    #     nbDappdePos[ref.p]+=1
    #     sys.stdout.write(toPrint)

    # print >>sys.stderr, "\t", totroles ,"roles après épuration, dont ", totrolesnnuls, "rôles non nuls"
    
    # for p in nbDappdePos.keys():
    #     print >>sys.stderr, p, nbDeRolesParPos[p]/nbDappdePos[p], nbDappdePos[p] 


if __name__ == "__main__":
    #if debug: print inspect.stack()[0][3]
    sys.exit(main())
