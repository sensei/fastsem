#!/usr/bin/python 
# -*- coding: utf-8 -*-

import sys 
import inspect 
import getopt 
import operator 


from myBFS import makeGraph
import Frame as frameLib, Chemins
from semMisc import lexicalUnitization

"""
 Créé le mercredi 25 février 2015.

 permet de lire un fichier conll donné en argument, et contient quelques fonctions utiles pour itérer sur les phrases d'un corpus ou renvoyer l'ensemble des frames

"""

def startWithNumber(line):
    l=line.split()
    if len(l)>1 and l[0].isdigit():
        return True
    else :
        return False

def printSentence(sent,out_stream=sys.stderr,splitted=True):
    for i in map(str,xrange(1,1+len(sent))):
        toprint="\t".join(sent[i]) if splitted else sent[i].strip()
        out_stream.write(toprint+"\n")
    out_stream.write("\n")

def SentenceIteratorFromConll(in_stream=sys.stdin,splitted=True):
    sent={}
    treatment=lambda x: map(lambda y : y.strip(),x.strip().split("\t")) if splitted else x
    
    for line in in_stream:
        if startWithNumber(line):
            sent[line.split()[0]]=treatment(line)
        else:
            yield sent 
            sent={}
    if len(sent)!=0:
        yield sent
        sent={}

def frameIteratorFromConll(in_stream=sys.stdin,withPathes=False,withNulRole=False,keepUnann=False,onlyOtherSense=False,dic_lu=None,lang="en"):
    
    def keepFrame(name,word,pos,keepUnann,onlyOtherSense,dic_lu,lang):
        if onlyOtherSense and name=="_":
            return False
        if "UNANN" in l[6] and not keepUnann: 
            return False
        if name=="_" and dic_lu and lexicalUnitization(word,pos,lang) not in dic_lu : 
            return False
        if name=="_" and not dic_lu:
            return False

        return True
        

    Frame=frameLib.FrameWithPathes if withPathes else frameLib.Frame
    sentIterator=SentenceIteratorFromConll(in_stream=in_stream)
    for sent in sentIterator:
        graph=makeGraph(sent,splitted=True)
        localFrames=[]
        framePosition=[]
        frameNulPosition=[]
        localFramesNul=[]
        forgetFrameCol=[]
        for i in map(str,xrange(1,len(sent)+1)):
            l=sent[i]
            kf=keepFrame(l[6],l[2],l[3],keepUnann,onlyOtherSense,dic_lu,lang)
            if not kf:
                if l[6]!="_":
                    forgetFrameCol.append(7+len(localFrames)+len(forgetFrameCol))
                continue
            if l[6]!="_": 
                #print >>sys.stderr, "####", l[6]
                framePosition.append(i)
                localFrames.append(Frame(l[6],l[2],l[3]))
            else: 
                frameNulPosition.append(i)
                localFramesNul.append(Frame("__fnul__",l[2],l[3]))
        #printSentence(sent)
        for l in sent:
            ancestry={}
            forgottenCol=0
            for col in xrange(7,len(sent[l])):
                if col in forgetFrameCol:
                    forgottenCol+=1
                    continue
                #if sent[l][col]=="_": continue
                if sent[l][col]=='_' : 
                    if not withNulRole: continue
                    role='__rnul__'
                else:
                    role=sent[l][col]
                if withPathes:
                    #print >>sys.stderr, framePosition, col, sent[l],forgetFrameCol
                    
                    path=Chemins.findPath(sent[l][0],framePosition[col-7-forgottenCol],ancestry,sent,splitted=True,onlyDeep=True,graph=graph)
                    localFrames[col-7-forgottenCol].addRole(sent[l][col],sent[l][2],sent[l][3],path,int(framePosition[col-7-forgottenCol])-int(l))
                else:
                    localFrames[col-7-forgottenCol].addRole(sent[l][col],sent[l][2],sent[l][3])
            if withNulRole:
                for j in range(len(frameNulPosition)):
                    role='__rnul__'
                    if withPathes:
                        #print >>sys.stderr, frameNulPosition, localFramesNul, j
                        path=Chemins.findPath(sent[l][0],frameNulPosition[j],ancestry,sent,splitted=True,onlyDeep=True,graph=graph)
                        localFramesNul[j].addRole(role,sent[l][2],sent[l][3],path,int(frameNulPosition[j])-int(l))
                    else:
                        localFramesNul[j].addRole(role,sent[l][2],sent[l][3])

        for i in range(len(localFrames)):
            yield localFrames[i],sent,int(framePosition[i])
        for i in range(len(localFramesNul)):
            yield localFramesNul[i],sent,int(frameNulPosition[i])

def getAllFramesFromConll(in_stream=sys.stdin,withPathes=False):
    Frame=frameLib.FrameWithPathes if withPathes else frameLib.Frame
    sentIterator=SentenceIteratorFromConll(in_stream=in_stream)
    frames=[]
    for sent in sentIterator:
        localFrames=[]
        framePosition=[]
        for i in map(str,xrange(1,len(sent)+1)):
            l=sent[i]
            if l[6]=="_": continue
            localFrames.append(Frame(l[6],l[2],l[3]))
            framePosition.append(i)
        for l in sent:
            ancestry={}
            for col in xrange(7,len(sent[l])):
                if sent[l][col]=="_": continue
                if withPathes:
                    localFrames[col-7].addRole(sent[l][col],sent[l][2],sent[l][3],Chemins.findPath(int(sent[l][0]),framePosition[col-7],ancestry,sent))
                else:
                    localFrames[col-7].addRole(sent[l][col],sent[l][2],sent[l][3])
        frames.extend(localFrames)
    return frames
    
if __name__ == "__main__":
    #if debug: print inspect.stack()[0][3]
    print >>sys.stderr, sys.argv[0], "ne peut être utilisé en standalone"
