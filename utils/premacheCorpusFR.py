#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
import sys
import inspect
import operator
import getopt
sys.path.append("../classesPython")
import conllIO as cio
from Chemins import findPath
from myBFS import makeGraph
"""

Prend en entrée un corpus conll avec cadres ainsi que le fichier contenant les unités lexicales et retourne une liste de chemins syntaxiques associés à chacune des unités lexicales.
Ici on considère le cadre nul ainsi que les rôles nuls!


"""
class Glob():
    """variables à transférer
    """
    
    def __init__(self):
        """
        """
        self.fic_text=sys.stdin
        self.fic_out=sys.stdout
        self.fic_lu=None
        self.fic_lex=None

        self.dico_lex=None
        self.dico_lu={}

        self.phrase={}

        self.withNulFrames=False
        self.LUWithPos=False

        self.onlyDeep=False
        self.othersense=False

    def ouvertureFichiers(self):
        if self.fic_text!=sys.stdin:
            self.stream_text=open(self.fic_text,"r")
        else:
            self.stream_text=sys.stdin

        self.stream_lu=open(self.fic_lu,"r")

        if self.fic_lex!=None:
            stream_lex=open(self.fic_lex,"r")
            self.lectureLexique()
            stream_lex.close()

        if self.fic_out!=sys.stdout:
            self.stream_out=open(self.fic_out,"w")
        else:
            self.stream_out=sys.stdout

    def fermetureFichiers(self):
        if self.fic_text!=sys.stdin:
            self.stream_text.close()
        self.stream_lu.close()

    def lectureLexique(self):
        self.lex_dico={}
        for i in self.stream_lex.readlines():
            i=i.replace("\n","").split("\t")
            #print i
            S.dico_lex[i[0]]=i[1]


mysplit=lambda x: map(lambda y : y.strip(),x.split("\t"))
def ajoutAuDico1(dico,cle1,q=1):
    if cle1 not in dico.keys():
        dico[cle1]=0
    dico[cle1]+=int(q)

def ajoutAuDico2(dico,cle1,cle2,q=1):
    if cle1 not in dico.keys():
        dico[cle1]={}
    if cle2 not in dico[cle1].keys():
        dico[cle1][cle2]=0
    dico[cle1][cle2]+=int(q)

def startWithNumber(line):
    l=line.split()
    if len(l)>1 and l[0].isdigit():
        return True
    else :
        return False

def newSentence(phrase):
    phrase.clear()
   
def lectureLU(S):
    for line in S.stream_lu.readlines():
        l=line.replace('\n','').split("\t")
        # if l[0].split(".")[1] not in ("a","n","v"):              #ici on ne teste que 3 POS
        #     continue
        S.dico_lu[l[0]]=l[1:]

# def findPath(FE,T,ancestry,phrase,splitted=False):
#     treatment=(lambda x: x) if splitted else (lambda x: mysplit(x))
#     if FE not in ancestry.keys():
#         ancestry[FE]=[]
#         q=FE
#         while q != '0' :
#             if q!=FE and q in ancestry.keys():
#                 ancestry[FE]=ancestry[q]+ancestry[FE]
#                 break
#             s=treatment(phrase[q])
#             ancestry[FE].insert(0,s[5])
#             q=s[4]
#     if T not in ancestry.keys():
#         ancestry[T]=[]
#         q=T
#         while q != '0' :
#             if q!=T and q in ancestry.keys():
#                 ancestry[T]=ancestry[q]+ancestry[T]
#                 break
#             s=treatment(phrase[q])
#             ancestry[T].insert(0,s[5])
#             q=s[4]

#     ancestryT=list(ancestry[T])
#     ancestryFE=list(ancestry[FE])

#     q=0
#     while q < len(ancestryFE) and q < len(ancestryT) and ancestryT[q]==ancestryFE[q] :
#         q+=1

#     ancestryT.reverse()
#     ancestryT=ancestryT[:len(ancestryT)-q]
#     ancestryT=["-"+ancestryT[i] for i in range(len(ancestryT))]
#     ancestryFE=ancestryFE[q:]
     
#     return "("+",".join(ancestryT+ancestryFE)+")"


# def surfaceDependency(govs,labels):
#     govs=govs.split("|")
#     labels=labels.split("|")
#     for i in xrange(len(govs)):
#         if not labels[i].startswith("D:"):
#             return govs[i],labels[i]
#     print >>sys.stderr, "pas de sytaxe de surface!", labels

# def deeepDependencies(govs,labels):
#     govs=govs.split("|")
#     labels=labels.split("|")
#     deepGovs=[]
#     deepLabels=[]
#     for i in xrange(len(govs)):
#         if labels[i].startswith("D:"):
#             deepGovs.append(govs[i])
#             deepLabels.append(labels[i])
#     return govs,labels

# def findPath(FE,T,ancestry,phrase,splitted=False):
#     treatment=(lambda x: x) if splitted else (lambda x: mysplit(x))
    
#     if FE not in ancestry.keys():
#         ancestry[FE]=[]
#         q=FE
#         while q != '0' :
#             if q!=FE and q in ancestry.keys():
#                 ancestry[FE]=ancestry[q]+ancestry[FE]
#                 break
#             s=treatment(phrase[q])
#             gov,label=surfaceDependency(s[4],s[5])
#             ancestry[FE].insert(0,(gov,label))
#             q=gov
#     if T not in ancestry.keys():
#         ancestry[T]=[]
#         q=T
#         while q != '0' :
#             if q!=T and q in ancestry.keys():
#                 ancestry[T]=ancestry[q]+ancestry[T]
#                 break
#             s=treatment(phrase[q])
#             gov,label=surfaceDependency(s[4],s[5])
#             ancestry[T].insert(0,(gov,label))
#             q=gov

#     ancestryT=list(ancestry[T])
#     ancestryFE=list(ancestry[FE])

#     s=treatment(phrase[FE])
#     govs,labels=deeepDependencies(s[4],s[5])
#     if T in govs:
#         return "("+labels[govs.index(T)]+")"
    
#     s=treatment(phrase[T])
#     govs,labels=deeepDependencies(s[4],s[5])
#     if FE in govs:
#         return "("+labels[govs.index(FE)]  +")"  

#     q=0
#     while q < len(ancestryFE) and q < len(ancestryT) and ancestryT[q]==ancestryFE[q] :
#         q+=1

#     if q==len(ancestryT)==len(ancestryFE):
#         q-=1

#     ancestryT=list(map(lambda x: x[1],ancestryT))
#     ancestryFE=list(map(lambda x: x[1],ancestryFE))
#     ancestryT.reverse()
#     ancestryT=ancestryT[:len(ancestryT)-q]
#     ancestryT=["-"+ancestryT[i] for i in range(len(ancestryT))]
#     ancestryFE=ancestryFE[q:]


#     return "("+",".join(ancestryT+ancestryFE)+")"
def brutLexicalUnitization(lemme,pos):
    if pos in ('JJ','JJR','JJS','LS'):
        lemme=lemme+".a"
    elif pos in ('NN','NNP','NNPS','NNS'):
        lemme=lemme+".n"
    elif pos in ('VB','VBD','VBG','VBN','VBP','VBZ','MD'):
        lemme=lemme+".v"
    elif pos in ('RB','RBR','RBS','WRB'):
        lemme=lemme+".adv"
    elif lemme in ("much","some","a few","little"):
        lemme=lemme+".art"
    elif lemme in ("so","since","for","because","while","but") :
        lemme=lemme+".c"
    elif lemme in ("yoohoo","hey","sh") :
        lemme=lemme+".intj"
    elif lemme in ("450","forty","six","fifty","four","fifteen","twenty","hundred","seventy","6500","ten","1200","eight","ninety","1000","seventy-four","five hundred","seven","two hundred","sixty","five","twenty-five","twenty-one","two","fifty-two","one","twelve","400","million","three","zero","billion","thirty"):
        lemme=lemme+".num"
    elif lemme in ("while","although"):
        lemme=lemme+".scon"
    elif pos in ('IN','TO') :
        lemme=lemme+".prep"
    else:
        lemme=lemme

    return lemme

def lexicalUnitization(lemme,pos,S):
    lu=brutLexicalUnitization(lemme,pos)

    if S.dico_lex!=None and lu not in S.dico_lu.keys() and lemme in S.dico_lex.keys():
        return brutLexicalUnitization(S.dico_lex[lemme],pos)

    return lu 

def extractLus(S):
    numFrameCourant=0       
    if S.onlyDeep:
        graph=makeGraph(S.phrase,splitted=False)
    else:
        graph=None
    for line in range(1,len(S.phrase)+1) :
        line=str(line)
        l=mysplit(S.phrase[line])

        lu=lexicalUnitization(l[2],l[3],S) if S.LUWithPos else l[2]
        if l[6] != '_' or (S.withNulFrames and  lu in S.dico_lu.keys()):

            if 'UNANN' in l[6]:
                continue

            if l[6]!='_': 
                numFrameCourant+=1

            fnum=l[0]
            trigger_frame=l[6]
            trigger_lemme=l[2]
            trigger_pos=l[3]
            
            chemin={}
            fe_lemme={}
            fe_pos={}
            fe_role={}
            ancestry={}

            rolesUtilises=set() ##truc pour éviter les rôles dupliqués

            for line2 in range(1,len(S.phrase)+1):
                line2=str(line2)
                l2=mysplit(S.phrase[line2])
                if line2 != line:
                    chemin[line2]=findPath(FE=line2,T=line,ancestry=ancestry,phrase=S.phrase,onlyDeep=S.onlyDeep,graph=graph)
                else:
                    chemin[line2]="()"
                fe_lemme[line2]=l2[2]
                fe_pos[line2]=l2[3]
                fe_role[line2]="__rnul__"
                if l2[numFrameCourant+6]!='_' and  l2[numFrameCourant+6] not in rolesUtilises: 
                    fe_role[line2]=l2[numFrameCourant+6]
                    #rolesUtilises.add(l2[numFrameCourant+6])

            #partie affichage
            if S.othersense: 
                toPrint=trigger_frame if l[6] not in ('_','Other_sense') else '__fnul__'
            else:
                toPrint=trigger_frame if l[6] != '_' else '__fnul__'
            toPrint+="\t"
            toPrint+=trigger_lemme
            toPrint+="\t"
            toPrint+=trigger_pos
            for fe in fe_lemme.keys():
                toPrint+="\t"
                toPrint+=fe_role[fe]
                toPrint+="\t"
                toPrint+=fe_lemme[fe]
                toPrint+="\t"
                toPrint+=fe_pos[fe]
                toPrint+="\t"
                toPrint+=chemin[fe]
            toPrint+="\n"

            S.stream_out.write(toPrint)

def lectureTexte(S):

    sentIterator=cio.SentenceIteratorFromConll(S.stream_text,splitted=False)
    l=0
    for S.phrase in sentIterator:
        l+=1
        print >>sys.stderr, "\r",l,"                    ",
        extractLus(S)

    print >>sys.stderr,"\n",


def afficheAide():
    print >>sys.stderr,"usage: ",sys.argv[0], "--text fulltext.train --lu unités-lexicales.txt [autres options]"
    print >>sys.stderr,"options:"
    print >>sys.stderr,"\t -h            : affiche ce message"
    print >>sys.stderr,"\t --out fic     : spécifie le fichier de sortie"
    print >>sys.stderr,"\t --lex fic     : fichier lexique"
    print >>sys.stderr,"\t --nul         : prend en compte les frames nuls si spécifié"
    print >>sys.stderr,"\t --lupos       : si le fichier des LU contient des POS (pour l'anglais)"
    print >>sys.stderr,"\t --deep        : pour n'utiliser que les dépendances deep"
    print >>sys.stderr,"\t--othersense   : remplace Other_sense par __fnul__"
    exit(0)

def gestionArguments(S):
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help","text=","out=","lu=","lex=","nul","lupos","deep","othersense"])
    except getopt.GetoptError:
        # print help information and exit:
        print sys.argv[1:]
        afficheAide()

    for o,a in opts:
        if o in ("-h", "--help"):
            afficheAide()
        if o == "--text":
            S.fic_text=a
        if o == "--out":
            S.fic_out=a
        if o == "--lu":
            S.fic_lu=a
        if o == "--lex":
            S.fic_lex=a
        if o == "--nul":
            S.withNulFrames=True
        if o == "--lupos":
            S.LUWithPos=True    
        if o == "--deep":
            S.onlyDeep=True
        if o == "--othersense":
            S.othersense=True

    if (S.fic_lu==None):
        afficheAide()

def main():
    S=Glob()
    gestionArguments(S)
    S.ouvertureFichiers()
    lectureLU(S)
    lectureTexte(S)
    S.fermetureFichiers()

if __name__ == "__main__":
    #if debug: print inspect.stack()[0][3]
    sys.exit(main())
        
