#!/usr/bin/python 
# -*- coding: utf-8 -*-

from __future__ import division 
import sys 
import inspect 
import getopt 
import operator 
try:
   import cPickle as pickle
except:
   import pickle


"""
 Créé le mardi 27 janvier 2015.

 permet d'avoir une matrice de confusion par trigger ou globale

"""

class ConfusionMatrix(object):
    def __init__(self):
        self.dico_wordTrueFunctionSelectedFunction={}
        self.dico_trueFunctionSelectedFunction={}
        self.set_allFunctions=set()

    def addOccurrence(self,word,trueFunction,selectedFunction):
        self.dico_wordTrueFunctionSelectedFunction.setdefault(word,{})
        self.dico_wordTrueFunctionSelectedFunction[word].setdefault(trueFunction,{})
        self.dico_wordTrueFunctionSelectedFunction[word][trueFunction].setdefault(selectedFunction,0)
        self.dico_wordTrueFunctionSelectedFunction[word][trueFunction][selectedFunction]+=1

        self.dico_trueFunctionSelectedFunction.setdefault(trueFunction,{})
        self.dico_trueFunctionSelectedFunction[trueFunction].setdefault(selectedFunction,0)
        self.dico_trueFunctionSelectedFunction[trueFunction][selectedFunction]+=1

    def printMatrix(self,dico,out_stream):
        trueFunctions=set(dico.keys())
        selectedFunctions=set()
        for f in trueFunctions:
            selectedFunctions|=set(dico[f].keys())

        allFunctions=sorted(selectedFunctions|trueFunctions)

        
        out_stream.write( "\t"+"\t".join(allFunctions)+"\n")
        for f in allFunctions:
            out_stream.write(f+"\t")
            if f not in dico.keys():
                out_stream.write("\t".join(['0']*len(allFunctions))+"\n")
                continue
            for f2 in allFunctions:
                if f2 not in dico[f].keys():
                    out_stream.write('0\t')
                    continue
                out_stream.write(str(dico[f][f2])+'\t')
            out_stream.write("\n")
        out_stream.write("\n")

    def printMatrices(self,out_stream=sys.stdout):
        self.printMatrix(self.dico_trueFunctionSelectedFunction,out_stream)

        words=sorted(self.dico_wordTrueFunctionSelectedFunction.keys())

        for t in words:
            out_stream.write("Mot "+t+"\n")
            self.printMatrix(self.dico_wordTrueFunctionSelectedFunction[t],out_stream)

    def saveMatrices(self,out_stream=sys.stdout):
        pickle.dump(self.dico_wordTrueFunctionSelectedFunction,out_stream)
        pickle.dump(self.dico_trueFunctionSelectedFunction,out_stream)

