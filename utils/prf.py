#!/usr/bin/python 
# -*- coding: utf-8 -*-

from __future__ import print_function 
import sys 
import inspect 
import getopt

"""
 Créé le lundi 13 avril 2015.

 class for calculate precision, recall and F-measure

"""
class PRF(object):
    def __init__(self,field=""):
        self.field=field
        
        self.correctlyAttributed_Count=0.0
        self.attributed_Count=0.0
        self.belongs_Count=0.0
        
        self.precision=0.0
        self.recall=0.0
        self.fmeasure=0.0
    
    def CalcPrecisionRecallFmeasure(self):
       try:
          self.precision=self.correctlyAttributed_Count/self.attributed_Count
       except ZeroDivisionError:
          self.precision=0.0
       try:
          self.recall=self.correctlyAttributed_Count/self.belongs_Count
       except ZeroDivisionError:
          self.recall=0.0
       try:
          self.fmeasure=(2*self.precision*self.recall)/(self.precision+self.recall)
       except ZeroDivisionError:
          self.fmeasure=0.0


    def __str__(self):
        return "%s\n\tPrecision: %f\tRecall: %f\tF-measure: %f\n"%(self.field,self.precision,self.recall,self.fmeasure)
        


if __name__ == "__main__":
    print("Doesn't work in standalone",file=sys.stderr)
    sys.exit(main())
