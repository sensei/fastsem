#-*- coding: utf-8 -*-

import fastsem as fs

'''
python exemple.py

demande les LUs, les models la langue et un fichier conll

renvoie un fichier avec du conll enrichi en frames.
'''


#sd=fs.SemanticDecoder("../semanticParser/learning/lexical_units.txt","../semanticParser/learning/models/","fr") ## Lexical units, model, langue(en, fr, it)

sd=fs.SemanticDecoder("LUs_FR.txt","models_FR/","fr") ## Lexical units, model, langue(en, fr, it)

it=fs.SentenceIteratorFromConll(open("0078.conll"))

sent=it.next()
t=sd.process_sentence(sent)
with open("0078.conll.out","w") as fout:
    fs.printSentence(t,fout)

    while t:
        try:
            sent=it.next()
            t=sd.process_sentence(sent)
            fs.printSentence(t,fout)
        except(StopIteration):
            break
