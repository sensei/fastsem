#!/usr/bin/python 
# -*- coding: utf-8 -*-

import sys 
import inspect 
import begin 
import os
try:
   import cPickle as pickle
except:
   import pickle
import time
import zmq
from multiprocessing import Pool

sys.path.append("./utils")
from semMisc import FileManager,coarsePoS
from Chemins import findPath,expandPath
from embeddings import ReadEmbeddingsFromStream 
from Frame import FrameWithPathes as Frame
from prf import PRF
from conllIO import SentenceIteratorFromConll, printSentence, frameIteratorFromConll
import matriceDeConfusion as mdc

sys.path.append("./utils/liblinear-1.96/python")
import liblinear as ll
import liblinearutil as llu



"""
 Créé le vendredi 23 octobre 2015.

 nouvelle version de apprentissageFR.py , permet de faire une apprentissage complexe en séparant l'analyse par triggers et l'apprentissage par frame

 Comment sont calculées les statistiques:
- On compte le frame juste si rien trouvé mais ref=nul
- Si le frame ref est nul:
-- on considère que tous les rôles sont nuls et on compare

TODO:
- ajouter le traitement à la volée comme dans l'ancien modèle
- comparer avec l'ancien fastsem
"""
class Glob(object):
    def __init__(self,mode,lang,file_lu,file_in,fold_model,file_emb,expand_pathes,output=None,file_out='stdout',file_pathes=None,additional_features_file=None,gold_frame=False,file_format="premache"):
        "docstring"
        self.mode=mode
        self.lang=lang
        self.gold_frame=gold_frame

        self.fold_model=fold_model

        self.file_format=file_format
        
        self.rnul="__rnul__"
        self.fnul="__fnul__"
        
        self.frames=[self.fnul]
        self.roles=[self.rnul]
        self.words=[]
        self.features=[]
        self.rolesByFrame={}

        self.expand_pathes=expand_pathes

        self.additional_features=self.loadAdditionalFeatures(additional_features_file)
        
        if self.mode=='decode':
            self.file_in=file_in
            self.file_out=file_out
            self.loadBasics()       
            self.loadPathesToConsider(file_pathes)
        
        if self.mode=='test':
            self.output=output
            if self.output:
                self.outputRef=open(file_in+".ref","w")
                self.outputHyp=open(file_in+".hyp","w")
            self.file_in=file_in
            self.rolesByFrame={}
            self.loadBasics()

        self.dic_lu=self.readLU(file_lu)
        self.embeddingsScoreByFrameRoleWordTuple=self.readEmbeddings(file_emb)

        if self.mode=='train':
            self.entries=self.readEntries(file_in,file_format)
            self.planeSaving()

        if self.mode=="onTheFly":
            self.loadBasics()
            self.loadPathesToConsider(file_pathes)
        

    def loadAdditionalFeatures(self,codeFile):
        if not codeFile:
          return None
        else:
            with open(codeFile) as f:
                code=f.read()
            return code

    def __del__(self):
        if self.mode == 'train':
            with open(self.fold_model+"/listFrames.pkl","w") as f:
                pickle.dump(self.frames,f)
        
            with open(self.fold_model+"/listRoles.pkl","w") as f:
                pickle.dump(self.roles,f)

            with open(self.fold_model+'/listFeatures.pkl',"w") as f:
                pickle.dump(self.features,f)
        if self.mode== 'test':
            if self.output:
                self.outputRef.close()
                self.outputHyp.close()

    def readLU(self,file_lu):
        dic_lu={}
        with open(file_lu,"r") as f:
            for line in f.readlines():
                l=line.replace('\n','').split("\t")
                dic_lu[l[0]]=list(set(l[1:]))
                self.frames.extend((frame for frame in l[1:] if frame not in self.frames))
        return dic_lu

    def extractFrameFromPreprocessed(self,line):
        l=line.strip().split('\t')
        try:
            idframe=self.frames.index(l[0])
        except ValueError:
            self.frames.append(l[0])
            idframe=len(self.frames)-1

        lu=lexicalUnitization(l[1],l[2],self.lang) 
        frame=Frame(idframe,l[1],l[2])

        self.rolesByFrame.setdefault(idframe,set())

        for i in xrange(3,len(l)):
            if (i-3)%4==0:
                try:
                    role=self.roles.index(l[i])
                except ValueError:
                    self.roles.append(l[i])
                    role=len(self.roles)-1
                if l[i]!=self.rnul:
                    self.rolesByFrame[idframe].add(role)
            elif (i-3)%4==1:
                acteur=l[i]
            elif (i-3)%4==2:
                PoS=l[i]
            else:
                if self.expand_pathes:
                    pathes=expandPath(l[i])
                else:
                    pathes=[l[i]]
                frame.addRole(role,acteur,PoS,pathes)
        
        return frame

    def readEntries(self,file_in,file_format):
        entries=[]
        nb_frame=0

        if file_format=="premache":
            with FileManager(file_in,std='stdin') as stream_in:
                for line in stream_in.readlines():
                    nb_frame+=1
                    print >>sys.stderr, '\r', nb_frame,
                    ref=self.extractFrameFromPreprocessed(line)
                    entries.append(ref)
            print >>sys.stderr, ""
            return entries
        
        with FileManager(file_in,std='stdin') as stream_in:
            for frame,sent,idx in frameIteratorFromConll(in_stream=stream_in,withPathes=True,withNulRole=True,dic_lu=self.dic_lu,lang=self.lang):
                nb_frame+=1
                print >>sys.stderr, '\r', nb_frame,
                try:
                    idframe=self.frames.index(frame.name)
                except ValueError:
                    self.frames.append(frame.name)
                    idframe=len(self.frames)-1
                frame2=Frame(idframe,frame.trigger,frame.PoS)
                self.rolesByFrame.setdefault(idframe,set())
            
                for r in frame.roles:
                    try:
                        role=self.roles.index(r.role)
                    except ValueError:
                        self.roles.append(r.role)
                        role=len(self.roles)-1
                    if r.role!=self.rnul:
                        self.rolesByFrame[idframe].add(role) 
                    if self.expand_pathes:
                        pathes=expandPath(r.path)
                    else:
                        pathes=[r.path]
                    frame2.addRole(role,r.lemma,r.PoS,pathes,position=r.position)
                entries.append(frame2)
                
                # with open("mysupertest.premache","a") as fichier:
                #     fichier.write("\t".join([frame.name,frame.trigger,frame.PoS]))
                #     for r in frame.roles:
                #         fichier.write("\t"+"\t".join([r.role,r.lemma,r.PoS,r.path]))
                #     fichier.write("\n")

        print >>sys.stderr, ""
        return entries

    def readEmbeddings(self,file_emb):
        try:
            with open(file_emb,"r") as f:
                return ReadEmbeddingsFromStream(f,self.frames,self.roles,self.words,10)  
        except (IOError,TypeError):
            return None
    
    def planeSaving(self):
        if not self.fold_model:
            self.fold_model="model_"+time.strftime("%d%m%Y_%H%M")
        if not os.path.isdir(self.fold_model):
            os.mkdir(self.fold_model)

    def loadBasics(self):
        with open(self.fold_model+"/listFrames.pkl","r") as f:
            self.frames=pickle.load(f)
        with open(self.fold_model+"/listRoles.pkl","r") as f:
            self.roles=pickle.load(f)
        with open(self.fold_model+"/listFeatures.pkl","r") as f:
            self.features=pickle.load(f)

    def loadPathesToConsider(self,file_pathes):
        try:
            with open(file_pathes,"r") as f:
               self.isByPoS=pickle.load(f)
               self.pathesToConsider=pickle.load(f)
        except (IOError,TypeError):
            self.pathesToConsider=None

    def saveClassifier(self,classifier,name,classifierType):
        fic=self.fold_model+"/"+classifierType+"_"+name+".model"
        llu.save_model(fic,classifier)

    def saveXY(self,X,Y,name,classifierType):
        with open(self.fold_model+"/"+classifierType+"_"+name+".XY.pkl","wb") as f:
           pickle.dump(X,f,pickle.HIGHEST_PROTOCOL)
           pickle.dump(Y,f,pickle.HIGHEST_PROTOCOL)

    def loadClassifier(self,name,classifierType):
        fic=self.fold_model+"/"+classifierType+"_"+name+".model"
        return llu.load_model(fic)


class Stats(object):
    def __init__(self,fnul,rnul,confu,confuName=""):
        self.prf_selectionFrameWNul=PRF(field="selection Frame including nul")
        self.prf_selectionRoleWNul=PRF(field="selection Role including nul")
        self.prf_detectionTrigger=PRF(field="detection trigger excluding nul elements")
        self.prf_selectionFrame=PRF(field="selection frame excluding nul elements")
        self.prf_detectionFE=PRF(field="detection FE excluding nul elements")
        self.prf_selectionRole=PRF(field="selection role excluding nul elements")

        self.useConfuMatrices=confu
        if self.useConfuMatrices:
            self.confuFrame=mdc.ConfusionMatrix()
            self.confuRole=mdc.ConfusionMatrix() # pour un (Frame-Chemin), les rôles 
            self.confuName=confuName
         
        self.fnul=fnul
        self.rnul=rnul

    def frame(self,hyp,ref,trigger,frames):
        isTrigger=1 if ref!=self.fnul else 0
        detectedTrigger=1 if hyp and int(hyp)!=self.fnul else 0

        self.prf_selectionFrameWNul.belongs_Count+=1 
        self.prf_selectionFrameWNul.correctlyAttributed_Count+=1 if (hyp and int(hyp)==ref) or (not hyp and ref==self.fnul) else 0
        self.prf_selectionFrameWNul.attributed_Count+=1      


        self.prf_detectionTrigger.belongs_Count+=isTrigger
        self.prf_detectionTrigger.attributed_Count+=detectedTrigger
        self.prf_detectionTrigger.correctlyAttributed_Count+=(isTrigger*detectedTrigger)

        self.prf_selectionFrame.belongs_Count+=isTrigger
        self.prf_selectionFrame.attributed_Count+=detectedTrigger
        self.prf_selectionFrame.correctlyAttributed_Count+=1 if isTrigger and detectedTrigger and ref==hyp else 0

        if self.useConfuMatrices:
            self.confuFrame.addOccurrence(trigger,frames[ref],(frames[int(hyp)] if hyp else frames[self.fnul]))

    def role(self,hyps,refs,frameIsCorrect,frame,roles,pathes):
        tot_isFE=0
        tot_detectedFE=0
        tot_isAndDetectedFE=0
        
        if hyps:                # Si frame nul on a une série de roles nuls dans l'hypothèse
            i=0
            for r in refs:
                isFE=detectedFE=0
                if r!=self.rnul:
                    isFE=1
                if int(hyps[i])!=self.rnul: 
                    detectedFE=1
                tot_isFE+=isFE
                tot_detectedFE+=detectedFE
                tot_isAndDetectedFE+=(isFE*detectedFE)

                self.prf_selectionRoleWNul.belongs_Count+=1
                self.prf_selectionRoleWNul.attributed_Count+=1
                self.prf_selectionRoleWNul.correctlyAttributed_Count+=1 if r==int(hyps[i]) else 0

                self.prf_detectionFE.belongs_Count+=isFE
                self.prf_detectionFE.attributed_Count+=detectedFE
                self.prf_detectionFE.correctlyAttributed_Count+=(isFE*detectedFE)

                self.prf_selectionRole.belongs_Count+=isFE
                self.prf_selectionRole.attributed_Count+=detectedFE
                self.prf_selectionRole.correctlyAttributed_Count+=1 if r==int(hyps[i]) and r!=self.rnul else 0

                if self.useConfuMatrices:
                    self.confuRole.addOccurrence(str((frame,pathes[i])),roles[r],roles[int(hyps[i])])

                i+=1

        else: # les frames sont différents
            for r in refs:
                self.prf_selectionRoleWNul.belongs_Count+=1
                self.prf_selectionRoleWNul.attributed_Count+=1
                if r==self.rnul:
                    pass
                #     self.prf_selectionRoleWNul.correctlyAttributed_Count+=1 # je ne suis pas sûr de ça
                else:
                    self.prf_detectionFE.belongs_Count+=1
                    self.prf_selectionRole.belongs_Count+=1

    def printStats(self,file_out=None,stream_out=sys.stdout,message=''):
        def realPrint(stream,mess):
            stream.write(mess+"\n\n")
            self.prf_selectionFrameWNul.CalcPrecisionRecallFmeasure()
            stream.write("\n{}".format(self.prf_selectionFrameWNul))
            self.prf_selectionRoleWNul.CalcPrecisionRecallFmeasure()
            stream.write("{}\n".format(self.prf_selectionRoleWNul))
            self.prf_detectionTrigger.CalcPrecisionRecallFmeasure()
            stream.write("{}".format(self.prf_detectionTrigger))
            self.prf_selectionFrame.CalcPrecisionRecallFmeasure()
            stream.write("{}".format(self.prf_selectionFrame))
            self.prf_detectionFE.CalcPrecisionRecallFmeasure()
            stream.write("{}".format(self.prf_detectionFE))
            self.prf_selectionRole.CalcPrecisionRecallFmeasure()
            stream.write("{}\n".format(self.prf_selectionRole))

        if file_out:
            with FileManager(file_out,append=True,mode="a") as f:
                realPrint(f,message)
            return
        realPrint(stream_out,message)

    def __del__(self):
        if self.useConfuMatrices:
            self.confuFrame.printMatrices(open('confuFrameMatrices.'+self.confuName+'.olv','w'))
            self.confuRole.printMatrices(open('confuRoleMatrices.'+self.confuName+'.olv',"w"))


def enLexicalUnitization(lemme,pos):
    if pos in ('JJ','JJR','JJS','LS'):
        lemme=lemme+".a"
    elif pos in ('NN','NNP','NNPS','NNS'):
        lemme=lemme+".n"
    elif pos in ('VB','VBD','VBG','VBN','VBP','VBZ','MD'):
        lemme=lemme+".v"
    elif pos in ('RB','RBR','RBS','WRB'):
        lemme=lemme+".adv"
    elif lemme in ("much","some","a few","little"):
        lemme=lemme+".art"
    elif lemme in ("so","since","for","because","while","but") :
        lemme=lemme+".c"
    elif lemme in ("yoohoo","hey","sh") :
        lemme=lemme+".intj"
    elif lemme in ("450","forty","six","fifty","four","fifteen","twenty","hundred","seventy","6500","ten","1200","eight","ninety","1000","seventy-four","five hundred","seven","two hundred","sixty","five","twenty-five","twenty-one","two","fifty-two","one","twelve","400","million","three","zero","billion","thirty"):
        lemme=lemme+".num"
    elif lemme in ("while","although"):
        lemme=lemme+".scon"
    elif pos in ('IN','TO') :
        lemme=lemme+".prep"
    else:
        lemme=lemme

    return lemme

def lexicalUnitization(lemme,pos,lang):

    if lang.lower().startswith("fr") or lang.lower().startswith('it'):
        return lemme
    elif lang.lower().startswith("en"):
        lu=enLexicalUnitization(lemme,pos)

        # if S.dico_lex!=None and lu not in S.dico_lu.keys() and lemme in S.dico_lex.keys():
        #     return enLexicalUnitization(S.dico_lex[lemme],pos)

        return lu 

    else:
        print >>sys.stderr, "Langue inconnue: ", S.lang
        exit()


def tryIncrement(dic,key):
    try:
        dic[key]+=1
    except KeyError:
        dic[key]=1

def getNumFeatByKey(features,key):
    try:
        return features.index(key)
    except ValueError:
        features.append(key)
        return len(features)-1

def getNumFeat(features,*args):
    key='|'.join(map(str,args))
    return getNumFeatByKey(features,key)


def makeTriggerIterator(entries):
    sortedEntries=sorted(entries,key=lambda f: f.trigger)
    
    prec=sortedEntries[0].trigger
    toPop=[]
    for frame in sortedEntries:
        if frame.trigger==prec:
            toPop.append(frame)
        else:
            prec=frame.trigger
            yield toPop
            toPop=[frame]

def extractTriggerFeatures(S,frame):
    x={}
    y=frame.name
    
    # tryIncrement(x,getNumFeat(S.features,'a',len(frame.roles))+1)
    # tryIncrement(x,getNumFeat(S.features,'a',frame.trigger)+1)
    #tryIncrement(x,getNumFeat(S.features,'a',frame.name)+1)
   

    
    # regular features
    tryIncrement(x,getNumFeat(S.features,'a',frame.trigger,coarsePoS(frame.PoS,S.lang))+1)

    for r in frame.roles:
        map(lambda p: tryIncrement(x,getNumFeat(S.features, 'aa',p,r.lemma,r.PoS)+1),r.path)
        map(lambda p: tryIncrement(x,getNumFeat(S.features,'b',p)+1),r.path)
        tryIncrement(x,getNumFeat(S.features,'c',r.lemma)+1)
        tryIncrement(x,getNumFeat(S.features,'d',r.PoS)+1)
        tryIncrement(x,getNumFeat(S.features,'d',coarsePoS(r.PoS,S))+1)

    

    # features similaires à celles du NN
    # 0 feature 0.525186 # 0.011068
    # a+ aa  0.710865 # 0.700926
    # a+ aa+ e  0.711769 #0.701152
    for r in frame.roles:
        if r.position in range(-2,3):
            #pass
            #words around
            tryIncrement(x,getNumFeat(S.features,"a",r.position,r.lemma)+1) #0.709510 #0.690309
            # FeaturePosAround,
            tryIncrement(x,getNumFeat(S.features,"aa",r.position,r.PoS)+1) #0.700926 #0.174384
        # FeatureAllPaths,
        #tryIncrement(x,getNumFeat(S.features,"b", r.path)+1) #0.636097 #0.000000 
        # FeatureAllWords,
        #tryIncrement(x,getNumFeat(S.features,"c", r.lemma)+1) #0.692568 #0.146149
        # FeatureAllWordsWithPos,
        #tryIncrement(x,getNumFeat(S.features,"d", r.lemma, r.PoS)+1) #0.695505 #0.131918
        
    # FeaturePosLocal,
    tryIncrement(x,getNumFeat(S.features,'e',frame.PoS)+1) # 0.694601 #0.114073
    

    return x,y

def learn1Trigger(tobreak):
   S,entries=tobreak
   X=[]
   Y=[]
   for frame in entries:
      x,y=extractTriggerFeatures(S,frame)
      X.append(x)
      Y.append(y)
   print >>sys.stderr, '\r', entries[0].trigger,"                                         ",

   classifier=ll.problem(Y,X)
   model=llu.train(classifier,"-q")
   #S.saveXY(X,Y,entries[0].trigger,"trigger")
   S.saveClassifier(model,entries[0].trigger,'trigger')

def learnTriggers(S): 
    triggerIterator = makeTriggerIterator(S.entries)
    runArgs=[]
    for entries in triggerIterator:
       runArgs.append((S,entries))

    p = Pool(2)
    p.map(learn1Trigger, runArgs)
    #learn1Trigger(S,entries)
    #print >>sys.stderr, '\r', entries[0].trigger,"                                         ",

    print >>sys.stderr, "\r                                                    "

def makeFrameIterator(entries):
    sortedEntries=sorted(entries,key=lambda f: f.name)
    
    prec=sortedEntries[0].name
    toPop=[]
    for frame in sortedEntries:
        if frame.name==prec:
            toPop.append(frame)
        else:
            prec=frame.name
            yield toPop
            toPop=[frame]

def extractFrameFeatures(S,frame):
    y=[]
    x=[]
    for r in frame.roles:
        y.append(r.role)
        xx={}
        x.append(xx)
  
        try:
            tryIncrement(x,getNumFeat(S.features,"emb",S.embeddingsScoreByFrameRoleWordTuple[(frame.name,r.role,S.words.index(r.lemma))]))
        except (KeyError,ValueError):
           pass

        map(lambda p: tryIncrement(xx,getNumFeat(S.features, 'b',p)+1),r.path)
        tryIncrement(xx,getNumFeat(S.features,'c',r.lemma)+1)
        tryIncrement(xx,getNumFeat(S.features,'d',r.PoS)+1)
        tryIncrement(xx,getNumFeat(S.features,'d',coarsePoS(r.PoS,S.lang))+1)
        tryIncrement(xx,getNumFeat(S.features,'d',frame.name)+1)
        tryIncrement(xx,getNumFeat(S.features,'e',frame.trigger)+1)

        tryIncrement(xx,getNumFeat(S.features,'f',frame.trigger,r.path)+1)

        # tryIncrement(xx,getNumFeat(S.features,"f",frame.PoS)+1) # augmente légèrement la surface mais fais baisser la profonde

        def pathlen(path):
            if "no_path" in path:
              return -1
            return expandPath(path)[0].count(",")+1

        # tryIncrement(xx,getNumFeat(S.features,"f",str(pathlen(r.path[0])))+1)

        try:
            exec S.additional_features in locals(), globals()
        except TypeError:
            pass

        # tryIncrement(xx,getNumFeat(S.features,'b',frame.name)+1)
        # tryIncrement(xx,getNumFeat(S.features,'c',frame.name,frame.trigger,frame.PoS,r.path,r.lemma,r.PoS))
        # tryIncrement(xx,getNumFeat(S.features,'d',frame.name,r.path)+1)
        # tryIncrement(xx,getNumFeat(S.features,'e',frame.name,r.lemma,r.PoS)+1)
        # tryIncrement(xx,getNumFeat(S.features,'f',frame.name,frame.trigger,frame.PoS)+1)
        # tryIncrement(xx,getNumFeat(S.features,'g',frame.name,r.path,r.lemma,r.PoS)+1)
        

    return x,y

def learn1Frame(tobreak):
   S,entries=tobreak
   X=[]
   Y=[]
   for frame in entries:
      x,y=extractFrameFeatures(S,frame)
      X.extend(x)
      Y.extend(y)
   print >>sys.stderr, '\r', S.frames[entries[0].name],"                                         ",
   classifier=ll.problem(Y,X)
   model=llu.train(classifier,"-q")
   #S.saveXY(X,Y,S.frames[entries[0].name],"frame")
   S.saveClassifier(model,S.frames[entries[0].name],'frame')

def learnFrames(S):
   frameIterator = makeFrameIterator(S.entries)
   runArgs=[]
   for entries in frameIterator:
      learn1Frame((S,entries))




   print >>sys.stderr, "\r                                               "

@begin.subcommand                                    
def train(lu,file_in='stdin',fold_model=None,emb=None,lang='en',expand_pathes=True,additional_features_file=None,gold_frame=False,file_format="premache"):
    "Entraine FastSem"
    S=Glob('train',lang,lu,file_in,fold_model,emb,expand_pathes,additional_features_file=additional_features_file,gold_frame=gold_frame,file_format=file_format)

    print >>sys.stderr, "Apprentissage des Triggers"
    if not S.gold_frame:
       learnTriggers(S)
    print >>sys.stderr, "Apprentissage des Frames"
    learnFrames(S)




def showFrame(S,frame,prediction):
    if not S.output:
        return
    S.outputRef.write("{}\t".format(S.frames[frame.name]))
    if prediction:
        S.outputHyp.write("{}\t".format(S.frames[int(prediction)]))
    else:
        S.outputHyp.write(S.fnul)

def showRoles(S,frame,predictions):
    if not S.output:
        return
    for i in xrange(len(frame.roles)):
        S.outputRef.write('{}|{}\t'.format(S.roles[frame.roles[i].role],frame.roles[i].lemma))
        S.outputHyp.write('{}|{}\t'.format(S.roles[int(predictions[i])],frame.roles[i].lemma))
    endOutputLines(S)

def endOutputLines(S):
    if not S.output:
        return
    S.outputRef.write("\n")
    S.outputHyp.write("\n")

def predictFrame(S,frame,x,y):

    p_labels=p_acc=p_vals=None
    try:
        # on peut avoir une erreur ici
        model=S.loadClassifier(frame.trigger,'trigger')
        p_labels,p_acc,p_vals=llu.predict(y,x,model)
    except AttributeError:
        print >>sys.stderr, 'lexical unit ',frame.trigger,' jamais observée dans le train'
    else: 
        #        S.confuFrame.addOccurrence(ref.trigger,S.frames[ref.name],(S.frames[yF[0]] if p_labels else 'None')) 
        #pass
        return p_labels[0]
    return S.frames.index(S.fnul)

def predictRoles(S,frame,x,y):
    p_labels=p_acc=p_vals=None
    try:
        model=S.loadClassifier(S.frames[frame.name],'frame')
        p_labels,p_acc,p_vals=llu.predict(y,x,model)
    except AttributeError:
        print >>sys.stderr, 'frame ', S.frames[frame.name],' jamais observée dans le train'
    else:
        return p_labels
    return [S.roles.index(S.rnul)]*len(frame.roles)

def testIterator(S,stream_in):
    if S.file_format=="premache":
        for line in stream_in:
            yield S.extractFrameFromPreprocessed(line)
    else:
        for frame,_,_ in frameIteratorFromConll(in_stream=stream_in,withPathes=True,withNulRole=True):
            try:
                idframe=S.frames.index(frame.name)
            except ValueError:
                S.frames.append(frame.name)
                idframe=len(S.frames)-1
            frame2=Frame(idframe,frame.trigger,frame.PoS)
            S.rolesByFrame.setdefault(idframe,set())

            for r in frame.roles:
                try:
                    role=S.roles.index(r.role)
                except ValueError:
                    S.roles.append(r.role)
                    role=len(S.roles)-1
                if r.role!=S.rnul:
                    S.rolesByFrame[idframe].add(role) 
                if S.expand_pathes:
                    pathes=expandPath(r.path)
                else:
                    pathes=[r.path]
                frame2.addRole(role,r.lemma,r.PoS,pathes,position=r.position)
            yield frame2

def runTest(S,stats):
    i=0
    #XYt=[]
    #XYf=[]
    S.classifModel=S.loadClassifier("all",'trigger')
    with FileManager(S.file_in,std='stdin') as stream_in:
        print >>sys.stderr, "phrase ", i
        i+=1
        for frame in testIterator(S,stream_in):
            if not S.gold_frame:
               x,y=extractTriggerFeatures(S,frame)
               predictedFrame=predictFrame(S,frame,(x,),(y,))
               #XYt.append((x,y,frame.trigger))
            else:
               predictedFrame=frame.name
            stats.frame(predictedFrame,frame.name,frame.trigger,S.frames)
            showFrame(S,frame,predictedFrame)

            #x,y=extractFrameFeatures(S,frame)
            #XYf.append((x,y,S.frames[frame.name]))

            if predictedFrame==S.frames.index(S.fnul):
                #print [str(S.roles.index(S.rnul))]*len(frame.roles)
                stats.role( [str(S.roles.index(S.rnul))]*len(frame.roles),(r.role for r in frame.roles),True,S.frames[frame.name],S.roles,list(r.path for r in frame.roles))
                endOutputLines(S)
            elif predictedFrame==frame.name and len(frame.roles):
                x,y=extractFrameFeatures(S,frame)
                predictedRoles=predictRoles(S,frame,x,y)
                stats.role(predictedRoles,(r.role for r in frame.roles),True,S.frames[frame.name],S.roles,[r.path for r in frame.roles])
                showRoles(S,frame,predictedRoles)
            else:
                stats.role(None,(r.role for r in frame.roles),False,S.frames[frame.name],S.roles,[r.path for r in frame.roles])
                endOutputLines(S)

    # with open("testVectors.XY.pkl","wb") as testVectors:
    #     pickle.dump(XYt,testVectors,pickle.HIGHEST_PROTOCOL)
    #     pickle.dump(XYf,testVectors,pickle.HIGHEST_PROTOCOL)

@begin.subcommand                                    
def test(lu,file_in='stdin',fold_model=None,emb=None,lang='en',confu=False,output=False,expand_pathes=True,additional_features_file=None,gold_frame=False,stats_file=None,message="",file_format="premache"):
    "Teste FastSem"
    S=Glob('test',lang,lu,file_in,fold_model,emb,expand_pathes,output=output,additional_features_file=additional_features_file,gold_frame=gold_frame,file_format=file_format)
    stats=Stats(S.frames.index(S.fnul),S.roles.index(S.rnul),confu,confuName=file_in)
    
    runTest(S,stats)

    stats.printStats(file_out=stats_file,message=message)    


def unsemanticizeSentence(sent):
    return {x:list(y[:6]) for x,y in sent.items()}

def setFrameSentence(sent):
    return {x:list(y[:6]+["_"]) for x,y in sent.items()}

def extractFrame(S,sent,line):
    f=Frame(None,sent[line][2],sent[line][3])
    ancestries={}
    for line2 in map(str,xrange(1,1+len(sent))):
        if line==line2: 
            chemin="()"
        else:
            chemin=findPath(line2,line,ancestries,sent,splitted=True)
            if ((S.pathesToConsider and ((S.isByPoS and chemin in S.pathesToConsider.get(sent[line][3], [])) 
                                        or (not S.isByPoS and chemin in S.pathesToConsider))) 
                or not S.pathesToConsider):
                f.addRole(None,sent[line2][3],sent[line2][3],chemin,position=line2)    
    return f

def addFrameToSentence(S,line,frame,sent):
    if frame.name=="_" or frame.name==None or frame.name==S.fnul:return
    map(lambda x: x.append("_"),sent.values())
    sent[line][6]=frame.name if frame.name else "_"
    for r in frame.roles:
        sent[r.position][-1]=r.role if r.role!=S.rnul and r.role  else "_"    

def treatTrigger(S,sent,line):
    f=extractFrame(S,sent,line)
    x,y=extractTriggerFeatures(S,f)
    f.name=int(predictFrame(S,f,(x,),[]))
    if f.name!=S.frames.index(S.fnul) and len(f.roles):
        x,y=extractFrameFeatures(S,f)
        predictedRoles=predictRoles(S,f,x,[])
        for i in range(len(predictedRoles)):
            f.roles[i].role=S.roles[int(predictedRoles[i])]
    f.name=S.frames[int(f.name)]
    print f.name
    return f

def decodeSentence(S,sent):
    newSent=setFrameSentence(sent)
    for line in map(str, xrange(1,1+len(sent))):
        lu=lexicalUnitization(sent[line][2],sent[line][3],S.lang)
        if lu not in S.dic_lu:
            continue
        frame=treatTrigger(S,sent,line)
        addFrameToSentence(S,line,frame,newSent)
    return newSent

def decodeCorpus(S):
    with FileManager(S.file_in,std='stdin',mode='r') as stream_in, FileManager(S.file_out,std='stdout',mode='w') as stream_out:
        sentIterator=SentenceIteratorFromConll(stream_in)
        for sent in sentIterator:
            sent=unsemanticizeSentence(sent)
            newSent=decodeSentence(S,sent)
            printSentence(newSent,stream_out)

@begin.subcommand                                    
def decode(lu,file_in='stdin',file_out='stdout',fold_model=None,file_pathes=None,emb=None,lang='en',expand_pathes=True,additional_features_file=None,gold_frame=False):
    "Décode du conll"
    S=Glob('decode',lang,lu,file_in,fold_model,emb,expand_pathes,file_out=file_out,file_pathes=file_pathes,additional_features_file=additional_features_file)
    decodeCorpus(S)

@begin.start
def main():
    pass



class SemanticDecoder(object):
   """
   Class loading a model for semantic parsing and using it
   """
   def __init__(self,LUFile,modelsFolder,lang):
      """
      Load models and Lexical Units List

      :param LUFile: File of lemmas that can be Lexical Units
      :param modelsFolder: Folder containing all models 
      :type LUFile: str
      :type modelsFolder: str
      """
      self.S=Glob("onTheFly",lang,LUFile,None,modelsFolder,None,False)

   def process_sentence(self,sentence,sent_format="conll"):
      """
      Semantic parse the sentence passed by arg

      :param sentence: sentence as a list of struct Word(text,lemma,pos,dep_gov,dep_label) or list of conll columns
      :type sentence: List
      :return: List of struct WordWithSemanticAnnotation(text,lemma,pos,dep_gov,dep_label,triggerOfWhichFrame,[roleIn1stFrame,roleIn2ndFrame,...])
      """
      if sent_format!="conll":
          conllSent=self.listWord2conll(sentence)
      else:
          conllSent=sentence
      # if HasCycle(conllSent):
      #    conllSentWithSemanticAnnotation=setFrameSentence(conllSent) 
      # else: 
      conllSentWithSemanticAnnotation=decodeSentence(self.S,conllSent)
      if sent_format!="conll":
          return self.conll2list_WordWithSemanticAnnotation(conllSentWithSemanticAnnotation)
      return conllSentWithSemanticAnnotation  
   
   def listWord2conll(self,sentence):
      return {str(i+1):[str(i+1)]+str(sentence[i+1]).strip().split("\t") for i in xrange(len(sentence))}

   def conll2list_WordWithSemanticAnnotation(self,conllSentWithSemanticAnnotation):
      return [WordWithSemanticAnnotation(conllSentWithSemanticAnnotation[i][1],
                                         conllSentWithSemanticAnnotation[i][2],
                                         conllSentWithSemanticAnnotation[i][3],
                                         conllSentWithSemanticAnnotation[i][4],
                                         conllSentWithSemanticAnnotation[i][5],
                                         conllSentWithSemanticAnnotation[i][6],
                                         conllSentWithSemanticAnnotation[i][7:])
              for i in map(str,xrange(1,1+len(conllSentWithSemanticAnnotation)))]
   
   
class Word(object):
   """
   Object representing a word and its morphology and syntax
   """
   def __init__(self,text,lemma,pos,dep_gov,dep_label):
      """
      :param text: word
      :param lemma: lemma
      :param pos: pos
      :param dep_gov: syntactic governor
      :param dep_label: syntactic dependency label
      """
      self.text=text
      self.lemma=lemma
      self.pos=pos
      self.dep_gov=dep_gov
      self.dep_label=dep_label


class WordWithSemanticAnnotation(Word):
   """
   Object representing a word with its morphology,syntax, and semantic in a sentence
   seealso:: class Word
   """
   def __init__(self,text,lemma,pos,dep_gov,dep_label,triggerOf,roleList):
      """
      :param text: word
      :param lemma: lemma
      :param pos: pos
      :param dep_gov: syntactic governor
      :param dep_label: syntactic dependency label
      :param triggerOf: if trigger, Frame which is triggered
      :param roleList: List of roles, for each Frame of the sentence in order of appearance
      """
      Word.__init__(self,text,lemma,pos,dep_gov,dep_label)
      self.triggerOf=triggerOf
      self.roleList=roleList
      
   def __str__(self):
       return "{}".format([self.text,self.lemma,self.pos,str(self.dep_gov),self.dep_label,self.triggerOf]+self.roleList)



